<?php

namespace App\Console\Commands;

use App\Ticker;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Redis;

class Trending extends Command
{
    use DispatchesJobs;
    protected $signature = "trending:sync";

    protected $description = "Cron task to pull data from StockTwits";

    public function handle()
    {
	    $token = env('STOCKTWITS_TOKEN', 'STOCKTWITS_TOKEN');
	    $url_header = "https://api.stocktwits.com/api/2/trending/symbols.json";
	    $url = $url_header."?access_token=".$token;
	    $stocktwits = curl_init();
	    curl_setopt($stocktwits, CURLOPT_URL, $url);
	    curl_setopt($stocktwits, CURLOPT_RETURNTRANSFER, true);
	    $response = json_decode(curl_exec($stocktwits),true);
	    //var_dump ($response);
	    $markets = ["NYSE","DJIA", "SPY","SPX","IXIC" ,"IWM", "QQQ", "LABU", "TQQQ", "DIA", "VIX", "TVIX" ,"TLT","UVXY", "EEM","IBB"];
	    $trendings =[];
	    $symbols = $response["symbols"];
	    for($i=0; $i< 10;$i++){
			$symbol = $symbols[$i];
			if (in_array($symbol['symbol'], $markets)){
				continue;
			}

			if (strpos($symbol['symbol'], ".") !== false){
				// if the symbol contains "." like $BTC.X, skip it!
				continue;
			}
			$checkSym = Ticker::find($symbol['symbol']);
			if (is_null($checkSym)){
				Ticker::create([
					"symbol"=> $symbol["symbol"],
					"title"=> $symbol["title"],
				]);
			}
			$trendings[] = ["symbol"=> $symbol['symbol'], "title"=> $symbol['title'], "id"=>$symbol['id']];
	    }
	    $trendings = json_encode($trendings);
	    Redis::set("trending", $trendings, "EX", 240);

    }	
}

?>


