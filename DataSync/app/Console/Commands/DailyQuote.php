<?php

namespace App\Console\Commands;

use App\Ticker;
use App\Daily;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Redis;
use DateTime;

class DailyQuote extends Command
{
    use DispatchesJobs;
    protected $signature = "daily:sync";

    protected $description = "Cron task to pull daily stock data from AlphaVantage";

    public function handle()
    {
        $url_header = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=";
        $token = env('ALPHA_TOKEN', 'ALPHA_TOKEN');
        
        $tickers = Ticker::all();
        $daily =[];

        foreach($tickers as $symbol){
            // $symbol = "AMZN";
            try{
                $symbol = $symbol->symbol;
                $url = $url_header.$symbol."&outputsize=compact&apikey=".$token;
                $alpha = curl_init();
                curl_setopt($alpha, CURLOPT_URL, $url);
                curl_setopt($alpha, CURLOPT_RETURNTRANSFER, true);
                $response = json_decode(curl_exec($alpha),true);
                $daily =[];
                if (isset($response["Time Series (Daily)"])){
                    $prices = $response["Time Series (Daily)"];
                    // var_dump($prices);
                    foreach($prices as $key=> $price){
                        $id =  hash("sha512",$key."_".strtoupper($symbol));
                        $temp =array(
                            "open"=> floatval($price["1. open"]),
                            "high"=> floatval($price["2. high"]),
                            "low"=> floatval($price["3. low"]),
                            "close"=> floatval($price["4. close"]),
                            "volume"=> $price["5. volume"],
                            "ticker" => strtoupper($symbol),
                            "date" => new DateTime($key),
                            "id"=> $id
                        );
            
                        $daily_col = Daily::find($id);
                        
                        if (is_null($daily_col)){
                            Daily::create($temp);
                        }
                        $daily[] = $temp;
                    }
                }
                
            }catch(Exception $e){
                echo ("Something is wrong ". $e->getMessage());
            }
            
            sleep(2);
        }  
        $daily = json_encode($daily);   
        // echo($daily);
	    // Redis::set("trending", $trendings, "EX", 240);

    }	
}

?>
