<?php

namespace App\Console\Commands;

use App\Ticker;
use App\Stock;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Redis;
use DateTime;

class Stocks extends Command
{
    use DispatchesJobs;
    protected $signature = "stock:sync";

    protected $description = "Cron task to pull 5 min stock data from AlphaVantage";

    public function handle()
    {
        // $url_header = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=";
        // $url_header = "https://www.alphavantage.co/query?function=BATCH_STOCK_QUOTES&symbols=";
        $url_header = "https://api.iextrading.com/1.0/stock/market/batch?symbols=";
        $token = env('ALPHA_TOKEN', 'ALPHA_TOKEN');
        
        $tickers = Ticker::all();
        $daily =[];
        
        $no_tickers = count($tickers);
        $no_rem_tickers = $no_tickers %100;
        $no_buckets = floor($no_tickers/100);

        if ($no_rem_tickers >0){
            $no_buckets+=1;
        }
       
        // $symbol = "AMZN";
       
        $percent = ceil($tickers->count()/$no_buckets);
        $partialCollections = $tickers->chunk($percent);
        // var_dump($partialCollections);

        foreach($partialCollections as $cols){
            $symbols = array();
            foreach($cols as $sym){
                $symbols[] = $sym->symbol;
            }
            $symbols = implode(",", $symbols);
            try{
                // $symbol = $symbol->symbol;
                // $url = $url_header.$symbols."&interval=5min&outputsize=compact&apikey=".$token;
                // $url = $url_header.$symbols."&apikey=".$token;
                $url = $url_header.$symbols."&types=chart&range=1d&last=5";
                $alpha = curl_init();
                curl_setopt($alpha, CURLOPT_URL, $url);
                curl_setopt($alpha, CURLOPT_RETURNTRANSFER, true);
                $response = json_decode(curl_exec($alpha),true);
                $daily =[];
                // AlphaVantage

                // if (isset($response["Stock Quotes"])){
                    // $prices = $response["Stock Quotes"]; // cached this one

                    // var_dump($prices);

                foreach($response as $key => $charts){
                    $latest_price = null;
                    $oldest_price = null;
                    foreach($charts["chart"] as $price){
                        // $symbol = $price["1. symbol"];
                        // $key = $price["4. timestamp"];
                        if (isset($price["close"]) && isset($price["open"])){
                            $date = $price["date"];
                            $min = $price["minute"];
                            $datetime = $date." ".$min;
                            $id =  hash("sha512",$datetime."_".strtoupper($key));
                            $temp =array(
                                "timestamp_str" => $key,
                                "open"=> floatval($price["open"]), // not valid anymore
                                "high"=> floatval($price["high"]), // not valid anymore
                                "low"=> floatval($price["low"]), // not valid anymore
                                "close"=> floatval($price["close"]),
                                "volume"=> (int) $price["volume"],
                                "ticker" => strtoupper($key),
                                "date" => DateTime::createFromFormat("Ymd H:i",$datetime),
                                "id"=> $id
                            );
                
                            $daily_col = Stock::find($id);
                            
                            if (is_null($daily_col)){
                                Stock::create($temp);
                            }
                            $daily[] = $temp;
                            $latest_price = $price;

                            if (empty($oldest_price)){
                                if ($price["low"]>=0 || $price["high"]>=0){
                                    $oldest_price = $price;
                                }
                               
                            }
                        }
                        
                    }
                    if (!is_null($latest_price)){
                        $latest_price["oldest"] = $oldest_price;
                        Redis::set($key, json_encode($latest_price), "EX", 900);
                    }
                   
                }
                    
                // }
                
                
            }catch(Exception $e){
                echo ("Something is wrong ". $e->getMessage());
            }
        }
    
        $daily = json_encode($daily);   
        // echo($daily);
	    

    }	
}

?>
