<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
class Stock extends Eloquent
{
    //
    protected $primaryKey ="id";
    protected $connection = 'mongodb';
    protected $dates = ["date"];
    protected $collection = "stocks";
    protected $fillable = ["id","open","close","volume","high","low","ticker","date"];
    public $incrementing = false;
}
