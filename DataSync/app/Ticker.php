<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticker extends Model
{
    //
    protected $primaryKey = "symbol";
    protected $fillable = ["symbol","title","created_at","updated_at"];
    protected $table = "tickers";
    public $incrementing = false;
}
