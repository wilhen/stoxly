from flask import Blueprint
from flask import render_template, url_for, request, redirect, make_response, jsonify
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token, get_jwt_identity,
    unset_jwt_cookies, set_access_cookies, jwt_optional
)
from app.models.Users import Users
# from app.database import session
# from app.database import db
import hashlib
from datetime import datetime, timedelta

login_blueprint = Blueprint('login', __name__)
jwt = JWTManager()

@login_blueprint.route("/login")
@jwt_optional
def index():
    if  get_jwt_identity() == None:
        return render_template('login.html')
    else:
        return make_response(redirect(url_for("main.index")))

@login_blueprint.route("/login", methods=["POST"])
def login():

    data = request.form
    
    email = data.get('email', None)
    password = data.get('password', None)

    if not email:
        return jsonify({"message": "Missing email parameter"}), 400
    if not password:
        return jsonify({"message": "Missing password parameter"}), 400

    # if input all good
    # fetch the correct user from db
    # uc = session.query(Users).filter_by(email=email)
    uc = Users.query.filter_by(email=email)

    if uc.count() == 0:
        # if user is not found
        return jsonify({"message": "Authentication failed"}), 400
    else:
        uc = uc.first()
        if uc.password != (hashlib.sha512((data["password"]).encode("utf-8"))).hexdigest():
            # incorrect authentication
            return jsonify({"message": "Authentication failed"}), 400

        # resp = jsonify({"access_token":access_token})
        access_token = generateAccessToken(uc.user_id)
        resp = make_response(redirect(url_for("main.index")))
        # we're dealing with website so save the token in cookie
        set_access_cookies(resp, access_token)
        return resp


@login_blueprint.route("/api/login", methods=["POST"])
def restLogin():
    
    data = request.form
    
    email = data.get('email', None)
    password = data.get('password', None)

    if not email:
        return jsonify({"message": "Missing email parameter"}), 400
    if not password:
        return jsonify({"message": "Missing password parameter"}), 400

    # if input all good
    # fetch the correct user from db
    # uc = session.query(Users).filter_by(email=email)
    uc = Users.query.filter_by(email=email)

    if uc.count() == 0:
        # if user is not found
        return jsonify({"message": "Authentication failed"}), 400
    else:
        uc = uc.first()
        if uc.password != (hashlib.sha512((data["password"]).encode("utf-8"))).hexdigest():
            # incorrect authentication
            return jsonify({"message": "Authentication failed"}), 400

        
        access_token = generateAccessToken(uc.user_id)
        resp = jsonify({"access_token":access_token})
        return resp


def generateAccessToken(user_id):
    
    expires = timedelta(days=30)
    access_token = create_access_token(identity=user_id, expires_delta=expires)
    return access_token

@login_blueprint.route('/logout', methods=['GET'])
def logout():
    # if the user wants to logout, just remove the token
    # resp = jsonify({'access_token': None}) # change this to redirect to login
    resp = make_response(redirect(url_for("login.index")))
    unset_jwt_cookies(resp)
    return resp
   

# @login_blueprint.route('/protected')
# @fresh_jwt_required
# # protected by jwt access token in cookie
# def protected():
#     # Access the identity of the current user with get_jwt_identity
#     current_user = get_jwt_identity()
#     return jsonify(logged_in_as=current_user)

def error_utils(error_message):
    
    resp = make_response(redirect(url_for("login.index", error=error_message)))
    unset_jwt_cookies(resp)
    return resp


