import pandas as pd
from pandas import read_csv, datetime, DataFrame
# import matplotlib.pyplot as plt
import statsmodels.api as sm
import numpy as np

from statsmodels.tsa.api import VAR, AR
from statsmodels.tsa.arima_model import ARIMA
from statsmodels.tsa.arima_model import ARIMAResults
# from matplotlib import pyplot
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.svm import SVC, SVR
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor
import dill
from sklearn.externals import joblib

# from pandas.tools.plotting import autocorrelation_plot
import redis


red = redis.StrictRedis(host="127.0.0.1", port=6379)

def __getnewargs__(self):
	return ((self.endog),(self.k_lags, self.k_diff, self.k_ma))
ARIMA.__getnewargs__ = __getnewargs__

def parser(x):
	return datetime.strptime(x, '%Y-%m-%d')


# needs to accept JSON array with aggregate sentiment per day exclude holiday
def arima_sent(sym ,dateset):

    series = []
    # change series
    # series = read_csv('./csv/Formula/sentiment_GE.csv', header=0, parse_dates=[0], index_col=0, squeeze=True, date_parser=parser)


    for i in dateset:

        series.append(i["aggregate"])

    series = np.array(series)
    # print(series.head()) # forgot about this

    # autocorrelation_plot(series)
    # pyplot.show()

    # fit model
    # model = ARIMA(series, order=(5,1,0))
    # model_fit = model.fit(disp=0)
    # print(model_fit.summary())
    # plot residual errors
    # residuals = DataFrame(model_fit.resid)
    # residuals.plot()
    # pyplot.show()
    # residuals.plot(kind='kde')
    # pyplot.show()
    # print(residuals.describe())

    X= series # change this one
   
   
   
    
    model = ARIMA(X, order=(1,1,0))
    model_fit = model.fit(disp=0)
    # output = model_fit.forecast()
    # yhat = output[0]
    # predictions.append(yhat)
    # obs = test[t]
    # history.append(obs)
    # print('predicted=%f, expected=%f' % (yhat, obs))

    model_fit.save("/home/william_hidayat12/event_detection_sentiment_analysis/testing_python/pickles/ARIMA/"+sym+"_arima.pkl")
    # error = mean_squared_error(test, predictions)
    # print('Test MSE: %.3f' % error)
    red.set(sym+"_sents", 1)
    red.expire(sym+"_sents", 600)
    return model_fit
    # plot
    # pyplot.plot(test)
    # pyplot.plot(predictions, color='red')
    # pyplot.show()

def demo_mode():
    
    df=pd.read_csv('/home/william_hidayat12/stock_sent_GE.csv')
    dates = df["date"]

    ln = df["ln"]
    return_ = df["return"]
    close = df["close"]
    open = df["open"]
    pos = df["pos"]
    neg = df["neg"]

    close_ = close.tolist()
    charts = close_
    return_ = return_.tolist()

    pos_ = pos.tolist()
    neg_ = neg.tolist()
    dates = dates.tolist()
    ln_ = ln.tolist()

    return dates, pos_, neg_, charts, close_, ln_, return_
    



