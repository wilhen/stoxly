from flask import Blueprint
from flask import render_template, url_for, request, jsonify
import json
import redis
import subprocess
import time
from app.models.Tickers import Tickers
# from app.database import session
from pymongo import MongoClient
from flask_jwt_extended import (
    jwt_required, get_jwt_identity
)
tools = Blueprint('tools', __name__)

red = redis.StrictRedis(host='localhost', port=6379)
client = MongoClient('10.152.0.3',27017)

db = client.stoxly


@tools.route("/sentiment/dashboard")
@jwt_required
def index():
    date = request.args.get('date', None)
    is_active = request.args.get('active', None)
    is_original = request.args.get('original', None)

    d = "2018-01-25"
    active = 0
    original = False
    if date!=None:
        d=date 


    if is_active!=None:
        active = int(is_active)

    if is_original !=None:
        original =True

    callScript(d, active)
    with open('/home/william_hidayat12/data.json') as data_file:    
        data = json.load(data_file)

    temp_res = []
    db2 = client.tweets
    collection2 = db2.djias
    sentiments = db.sentiments
    bearish = 0
    bullish =0 
    spam_no = 0
    neutral = 0
    temp = {}
    print ("no outlier ", len(data))
    for i in data:
        idx = str(i["id"])
        # print (idx)
        textset = collection2.find_one({"id":idx})
        source = textset["status"]["source"]
        # print (textset)
        if textset["status"]["truncated"] == False:
            text = textset["status"]["text"]
        else:
            text = textset["status"]["extended_tweet"]["full_text"]

        d = dict()
        d["id"]  = idx
        d["text"] =  text
        # d["token"] = i["tokens"]
        d["source"] = source
        query = sentiments.find_one({"id":idx})
        spams = db.spams
        spam = spams.find_one({"id":idx, "spam":True})

        if original:

            if query !=None:
                if query["sentiment"] == "neutral":
                    d["sentiment"]= i["sentiment"]
                    d["edited"] = True
                else:
                    d["sentiment"]= query["sentiment"]
                    d["edited"] = True
        
            elif spam != None:
                d["sentiment"]= i["sentiment"]
                d["edited"] = True 

            else:
                # from the algorithm
                d["sentiment"]= i["sentiment"]
                d["edited"] = False
        else:
            # editing mode
            if query !=None:
                
                d["sentiment"]= query["sentiment"]
                d["edited"] = True
            
            else:
                spams = db.spams
                spam = spams.find_one({"id":idx, "spam":True})
                if spam == None:
                    # from the algorithm
                    d["sentiment"]= i["sentiment"]
                    d["edited"] = False
                    
                else:
                    # it is spam
                    d["sentiment"]= "spam"
                    d["edited"] = True
                    spam_no+=1
        
        if d["sentiment"] == "bearish":
            bearish+=1
        elif d["sentiment"] == "bullish":
            bullish+=1
        elif d["sentiment"] == "neutral":
            neutral+=1
        
        
        temp_res.append(d)

    
    temp["bearish_no"]= bearish
    temp["bullish_no"]= bullish
    temp["spam_no"] = spam_no
    temp["neutral_no"] = neutral
   

    return render_template('sentiment_dashboard.html', data = temp_res, stat=temp)

def callScript(date="2018-01-25", active=0):
   
    subprocess.check_call("cd /home/william_hidayat12/event_detection_sentiment_analysis/testing_python/; python SentimentAnalyse.py --date="+date+" --active="+str(active), shell=True)
   
@tools.route("/sentiment", methods=["PUT"])
def edit():
    collection = db.sentiments
    spams = db.spams
    req = request.get_json(force=True)
    
    if req==None:
        return jsonify({"message":"Invalid request"}), 400    
    
    if "sentiment" not in req:
        return jsonify({"message":"Invalid argument"}), 400
    
    if "id" not in req:
        return jsonify({"message":"Invalid argument"}), 400
    
    sentiment = req["sentiment"]
    d = dict()
    d["id"] = req["id"]
    d["sentiment"] = sentiment
    

    if collection.find({"id":req["id"]}).count() ==0:
        try:
            # if the sentiment is defined as spam, remove it to allow update
            # if done, insert it
            # if not, catch the error and pass it  
            spams.find_one_and_delete({"id": req["id"], "spam":True})
        except Exception:
            pass
        collection.insert(d)
        return jsonify({"message":"Sentiment recorded"}), 201
    else:
        # update sentiment
        col = collection.find_one({"id":req["id"]})
        # if the recorded sentiment and the new sentiment are different
        # if not, throw error
        
        if col["sentiment"] == sentiment:
            return jsonify({"message":"Sentiment has been recorded"}), 400
        else:
            # if yes, then update it
            # just in case
            try:
                # if the sentiment is defined as spam, remove it to allow update
                # if done, insert it
                # if not, catch the error and pass it  
                spams.find_one_and_delete({"id": req["id"], "spam":True})
            except Exception:
                pass
            
            collection.find_one_and_update({"id": req["id"]}, {"$set":{"sentiment":sentiment}})
            return jsonify({"message":"Sentiment updated"}), 201

@tools.route("/spam", methods=["PUT"])
def spam():
    collection = db.spams
    req = request.get_json(force=True)
    sentiments = db.sentiments
    
    if req==None:
        return jsonify({"message":"Invalid request"}), 400    
    
    if "spam" not in req:
        return jsonify({"message":"Invalid argument"}), 400

    if "id" not in req:
        return jsonify({"message":"Invalid argument"}), 400
    
    spam = req["spam"]
    d = dict()
    d["id"] = req["id"]
    d["spam"] = spam
    if collection.find({"id":req["id"], "spam":True}).count() ==0:
        try:
            # if it is recorded as sentiment before, delete that one
            # else, pass it
            sentiments.find_one_and_delete({"id": req["id"]})
        except Exception:
            pass
        collection.insert(d)
        return jsonify({"message":"Spam recorded"}), 201
    else:
        return jsonify({"message":"Spam has been recorded"}), 400
