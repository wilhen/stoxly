from flask import Blueprint
from flask import render_template, url_for, request, jsonify
from app.models.ShortlistedStocks import ShortlistedStocks
# from app.database import session
# from app.database import db as rdb
import urllib3
import json
import redis
import locale
import subprocess
from pymongo import MongoClient, ASCENDING, DESCENDING
from datetime import datetime, timedelta
import hashlib
import time
import pytz
from tzlocal import get_localzone 
import math
from app.controllers.AnalyticsController import demo_mode
from app.controllers.AnalyticsController import arima_sent, __getnewargs__ as newargs
# from app.controllers.PricePrediction import newPricePredictionImpl
from statsmodels.tsa.arima_model import ARIMAResults
from statsmodels.tsa.arima_model import ARIMA

from flask_jwt_extended import (
    jwt_required, get_jwt_identity
)

main = Blueprint('main', __name__)
red = redis.StrictRedis(host='localhost', port=6379)
locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

# client = MongoClient('localhost',27017)
client = MongoClient('10.152.0.3',27017)

db = client.stoxly
collection = db.stocks

@main.route("/")
@jwt_required
def index():
    # s = ShortlistedStocks.query.all()
   
    user_id = get_jwt_identity()
    # session.commit()
    # s = session.query(ShortlistedStocks).filter_by(user_id=user_id)
    s = ShortlistedStocks.query.filter_by(user_id=user_id)
    
    return render_template('index.html', tickers = s)

@main.route("/trending")
def getTrendingStocks():
    
    """
        data should be cached at least 4 minutes
    """

    if (red.get("trending")==None):

        # Just in case of data lost, retry
        ACCESS_TOKEN = "97a1992ae3cd49c4fde95a153758153a877efe85" # hide this
        URL_HEADER = "https://api.stocktwits.com/api/2/trending/symbols.json"
        url = URL_HEADER+"?access_token="+ACCESS_TOKEN
        http = urllib3.PoolManager()

        response = http.request('GET', url)
        data = json.loads(response.data)
        symbols = data["symbols"]
        
        trendings = []
        markets = ["NYSE","DJIA", "SPY","SPX","IXIC" ,"IWM", "QQQ", "LABU", "TQQQ","DIA", "VIX", "TVIX" ,"TLT", "UVXY","IBB", "EEM"]
        for i in symbols:
            d = dict()
            if ("." in i["symbol"]) or (i["symbol"] in markets):
                continue
            d["symbol"]= i["symbol"]
            d["title"] = i["title"]
            d["id"] = i["id"]
            trendings.append(d)

        trendings = json.dumps(trendings)
        red.set("trending", trendings)
        red.expire('trending', 240) # 4 mins
       
        return jsonify(trendings)
    else:

        trendings = red.get("trending")
        trendings = json.loads(trendings)
        return jsonify(trendings)

@main.route("/quote/<symbol>")
def getIntraDay(symbol):

    if red.get(symbol)==None:
        # in case the data is not in the database
        # url = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol="+symbol+"&interval=5min&outputsize=compact&apikey="+API_KEY
        url = "https://api.iextrading.com/1.0/stock/market/batch?symbols="+symbol+"&types=chart&range=1d&last=5"
        http = urllib3.PoolManager()
        
        response = http.request('GET', url)
        data = json.loads(response.data) # python object transformation
       
        timeseries = data[symbol]["chart"]
        
        """
            It is ordered in sequence oldest - newest
            So choose the last one
        """

        latest_quote = dict()
        latest_ts = timeseries[-1]
        

        if latest_ts["high"] <0 or latest_ts["low"]<0:
            return  jsonify({"5. volume": 0, "6. change":0,"1. open":0,"4. close":0 })


        # find the first time trading
        oldest = dict()
        for i in timeseries:
            if i["high"] >=0 or i["low"]>=0:
                oldest["close"] = i["close"]
                oldest["open"] = i["open"]
                oldest["volume"] = i["volume"]
                ts = i["date"]+ " "+ i["minute"] 
                # oldest["timestamp"] = datetime.strptime(ts,"%Y%m%d %H:%M")
                break


        latest_ts["oldest"] = oldest
        
        ts = json.dumps(latest_ts) # json serialise
        red.set(symbol, ts)
        red.expire(symbol, 900) # should be only 4 mins
        
        closing = latest_ts["close"]
        opening = latest_ts["open"]
        volume = latest_ts["volume"]

        timestamp = latest_ts["date"]+ " "+ latest_ts["minute"] 
        timestamp = datetime.strptime(timestamp,"%Y%m%d %H:%M")


        latest_quote = dict()
        latest_quote["4. close"] = float(closing)
        latest_quote["1. open"] = float(opening)
        latest_quote["2. adjusted_opening"] = float(oldest["open"])
        adjusted_opening = oldest["open"]

        latest_quote["5. volume"] = locale.format("%d", float(volume), grouping=True) # make a comma seperated thousands
        latest_quote["6. change"] = (float(closing)- float(adjusted_opening))/float(adjusted_opening)*100
        latest_quote["6. change"] = round(latest_quote["6. change"],3)
        
        for i in timeseries:
            # tick = i # i = 2018-04-02 10:15:00
            if i["high"] <0 or i["low"]<0:
                continue

            d = dict()
            dt =  i["date"]+ " "+ i["minute"] 
            d["date"] = datetime.strptime(dt, "%Y%m%d %H:%M")
            d["ticker"] = symbol
            d["open"] = float(i["open"])
            d["close"] = float(i["close"])
            d["high"] = float(i["high"])
            d["low"] = float(i["low"])
            d["volume"] = float(i["volume"])

            tid = dt+"_"+symbol
            m = hashlib.sha512(tid.encode("utf-8"))
            d["id"] = m.hexdigest()
            if collection.find({"id":d["id"]}).count() ==0:
                collection.insert(d)

        return jsonify(latest_quote)
    
    else:
        # cached
    
        timeseries = red.get(symbol)
        timeseries = json.loads(timeseries)
       
        closing = timeseries["close"]
        opening = timeseries["open"]
        volume = timeseries["volume"]
        timestamp = timeseries["date"]+ " "+ timeseries["minute"] 
        timestamp = datetime.strptime(timestamp,"%Y%m%d %H:%M")

        if "oldest" in timeseries:
            oldest = timeseries["oldest"]
            adjusted_opening = oldest["open"]
        else:
            adjusted_opening = opening
       

        latest_quote = dict()
        latest_quote["4. close"] = float(closing)
        latest_quote["2. adjusted_opening"] = float(adjusted_opening)
        latest_quote["1. open"] = float(opening)
        latest_quote["6. change"] = (float(closing)- float(adjusted_opening))/float(adjusted_opening)*100
        latest_quote["6. change"] = round(latest_quote["6. change"],3)
        latest_quote["5. volume"] = locale.format("%d", float(volume), grouping=True) # make a comma seperated thousands
        return jsonify(latest_quote) #dump it to json string to flush it

@main.route("/prices/<symbol>")
def getPrices(symbol):
    # set default date as today
    # and end date as tomorrow

    # print (datetime.now().date())
    
    fq = request.args.get("frequency", None)
    dateReq = request.args.get("date", None) # manual date parameter
    
    if dateReq == None:
        date = datetime.now().date()
        dateStr = date.strftime("%Y-%m-%d")
    else:
        dateStr = dateReq
    
    
    temp = timeSeriesStocksImpl(symbol, dateStr, dateReq=dateReq, fq=fq)
    
   
    return jsonify(temp)


def timeSeriesStocksImpl(symbol, dateStr, dateReq=None, fq=None):

    lt = "$lt"
    cols = None
    if fq == None or fq == "daily":
 
        start = dateStr + " 00:00:00"
        # end = "2018-04-03 00:00:00"
        startDt = datetime.strptime(start,"%Y-%m-%d %H:%M:%S")
        endDt = startDt+timedelta(days=1)
       
        if dateReq == None:
           
            while True:
               
                cols = collection.find({"ticker":symbol, "$and":[{"date":{"$gte":startDt}},{"date":{lt:endDt}}]}).sort([("date",ASCENDING)])
              
                if cols.count()==0:
                    endDt = startDt
                    startDt = startDt-timedelta(days=1)
                else:
                    break
        else:
           
            cols = collection.find({"ticker":symbol, "$and":[{"date":{"$gte":startDt}},{"date":{lt:endDt}}]}).sort([("date",ASCENDING)])
            
    elif fq =="weekly":
        print(dateStr)
        """
            0: Monday
            6: Sunday
            max : 4 # Friday
            min : 0 # Monday

            monday == startDt
            friday == endDt
        """
        td = dateStr
        tdDt = datetime.strptime(td,"%Y-%m-%d")
        days = tdDt.weekday()
        # get Monday
        startDt = tdDt-timedelta(days=days)
        endDt = tdDt+timedelta(days=(4-days))

    else:
        # monthly
        end = dateStr + " 23:59:59"
        endDt = datetime.strptime(end,"%Y-%m-%d %H:%M:%S")
        startDt = endDt-timedelta(days=30)
    
    
    if fq == "weekly" or fq =="monthly":
        lt= "$lte"
        
        daily_collection = db.stocks_daily
        cols = daily_collection.find({"ticker":symbol, "$and":[{"date":{"$gte":startDt}},{"date":{lt:endDt}}]}).sort([("date", ASCENDING)])
    # else:
    #     cols = collection.find({"ticker":symbol, "$and":[{"date":{"$gte":startDt}},{"date":{lt:endDt}}]}).sort([("date",ASCENDING)])

    temp = []
    for i in cols:
        d = dict()
        # if fq == "monthly" or fq =="weekly":
        #     d["date"] = (i["date"]).strftime("%Y-%m-%d")
        # else:
        d["date"] = (i["date"]).strftime("%Y-%m-%d %H:%M:%S")
 
        d["ticker"] = i["ticker"]
        d["open"] = i["open"]
        d["close"] = i["close"]
   
        temp.append(d)
    # temp = sorted(temp, key = lambda x: x["date"], reverse=False)

    return temp

def sentimentAnalysisImpl(symbol, date):

    sentimentAnalysis = client.SentimentAnalysis # new db
    dailySentiment = sentimentAnalysis.daily_sentiment
    now = date.strftime("%Y-%m-%d")
    now_end = date + timedelta(days=1)
    now_end = now_end.strftime("%Y-%m-%d")
    todayStart = now + " 00:00:00"
    todayEnd = now_end + " 00:00:00"
    todayStartDt = datetime.strptime(todayStart,"%Y-%m-%d %H:%M:%S")
    todayEndDt = datetime.strptime(todayEnd,"%Y-%m-%d %H:%M:%S")
    sentiments = dailySentiment.find({"symbol":symbol, "$and":[{"created_at":{"$gte":todayStartDt}}, {"created_at":{"$lt":todayEndDt}}]})
    positives = 0
    negatives = 0
    for i in sentiments:
        positives = positives + i["positives"]
        negatives = negatives + i["negatives"]

    return positives, negatives, 

@main.route("/analytics/<symbol>")
def getFullData(symbol):

    print (symbol)
    date = datetime.now().date()
    dateStr = date.strftime("%Y-%m-%d")
    stocks = timeSeriesStocksImpl(symbol, dateStr) # charts

    """ Present price -- Access Cached """
    timeseries = red.get(symbol)
    timeseries = json.loads(timeseries)
    
    closing = timeseries["close"]
    if "oldest" in timeseries:
        opening = timeseries["oldest"]["open"]
    else:
        opening = timeseries["open"]
    volume = timeseries["volume"]
    timestamp = timeseries["date"]+ " "+ timeseries["minute"] 
    timestamp = datetime.strptime(timestamp,"%Y%m%d %H:%M")

    latest_quote = dict()
    latest_quote["close"] = float(closing)
    latest_quote["open"] = float(opening)
    latest_quote["change"] = (float(closing)- float(opening))/float(opening)*100
    latest_quote["change"] = round(latest_quote["change"],3)
    latest_quote["volume"] = locale.format("%d", float(volume), grouping=True) # make a comma seperated thousands
    """ """
    # pred, ret, pos_date, neg_date

    positives, negatives = sentimentAnalysisImpl(symbol, date)

    if red.get("new_pred_"+symbol) == None:
        pred, ret = newPricePredictionImpl(symbol)
        d = dict()
        d["pred_price"] = pred
        d["pred_return"] = ret
        d = json.dumps(d)
        red.set("new_pred_"+symbol, d)
        red.expire("new_pred_"+symbol, 300)

    else:
        d = red.get("new_pred_"+ symbol)
        d= json.loads(d)
        pred = d["pred_price"]
        ret = d["pred_return"]

    sentiments = dict()
    sentiments["positives"] = positives
    sentiments["negatives"] = negatives

    if pred == None:
        return jsonify({"sentiment": sentiments, "latest_quote": latest_quote, "charts":stocks, "prediction_price":float(0.0), "prediction_return": float(0.0)})

    pred = float(pred)
    ret = float(ret)
    
    
    
    return jsonify({"sentiment": sentiments, "latest_quote": latest_quote, "charts":stocks, "prediction_price":pred, "prediction_return": ret})


# @main.route("/forecast/sentiment/<symbol>")
# def predictSentimentInterface(symbol, start=None, forecast_period=1):
#     result = predictSentiment(symbol, start, forecast_period)
#     return jsonify(result)

# def predictSentiment(symbol, start=None, forecast_period=1):

#     if start == None:
#         date = datetime.now().date() # today as start

#     weekday = date.weekday()
#     isWeekday = date.weekday() <5

#     if not isWeekday:
#         date = date - timedelta(days = (weekday-4))

#     aggrs = []
#     print (date)
#     if red.get(symbol+"_sents") == None:
#         while True:
#             # brute search
#             pos, neg = sentimentAnalysisImpl(symbol, date)

#             aggr = math.log((1+pos)/(1+neg))
#             temp = {}
#             temp ["pos"] = pos
#             temp ["neg"] = neg
#             temp ["aggregate"] = aggr
#             temp ["date"] = date
#             aggrs.append(temp)
#             date = date - timedelta(days=1) # yesterday
#             weekday = date.weekday()
#             isWeekday = date.weekday() <5

#             if not isWeekday:
#                 date = date - timedelta(days = (weekday-4))
            
#             if date < datetime(2018,5,15).date():
#                 break
#         print (aggrs)
#         model = arima_sent(symbol, aggrs)

#     else:
#         ARIMA.__getnewargs__ = newargs

#         # load model
#         model = ARIMAResults.load('/home/william_hidayat12/event_detection_sentiment_analysis/testing_python/pickles/ARIMA/'+symbol+'_arima.pkl')

#     forecast = model.forecast(forecast_period)
#     print (forecast[0][forecast_period-1])
#     return {"aggregate_sentiment": forecast[0][forecast_period-1]}

def pricePredictionImpl(symbol, date=None, score=None):
    ## python backend uses UTC
    local = pytz.timezone("US/Eastern")
    date_ = False
    if date == None:
        date = datetime.now().date() # today
    else:
        date_ = True


    date_str = date.strftime("%Y-%m-%d")
    date_str = date_str + " 00:00:00"

    date = datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")
    
    tomorrow  = date + timedelta(days=1) # tomorrow
    
    # before_yesterday = yesterday - timedelta(days=1)
    daily_collection = db.stocks_daily

    # t_cols = daily_collection.find({"ticker":symbol,"$and":[{"date":{"$lt": tomorrow}}, {"date":{"$gte":date}}]})
    # # check if today closing price exists
    # if t_cols.count() == 0:
    #     while True:
    #         tomorrow = date
    #         date = date - timedelta(days=1)
    #         t_cols = daily_collection.find({"ticker":symbol,"$and":[{"date":{"$lt": tomorrow}}, {"date":{"$gte":date}}]})
    #         if t_cols.count() >0:
    #             break 
    
    
    yesterday = date - timedelta(days=7) # yesterday
    # y_cols = daily_collection.find({"ticker":symbol,"$and":[{"date":{"$lt": date}}, {"date":{"$gte":yesterday}}]})
    # if y_cols.count() == 0:
    #     # in case of public holiday or some random weekends
    #     while True:
    #         yesterday = yesterday - timedelta(days=1)
    #         date =  date - timedelta(days=1)
    #         y_cols = daily_collection.find({"ticker":symbol,"$and":[{"date":{"$lt": date}}, {"date":{"$gte":yesterday}}]})
    #         if y_cols.count() >0 :
    #             break
    if score != None:
        subprocess.check_call("cd /home/william_hidayat12/event_detection_sentiment_analysis/testing_python; python3 RegressionModelAbstraction.py --symbol="+symbol+"_score --score="+str(score), shell=True)
        pred = float(red.get(symbol+"_score_regress"))
        pred = float("{0:.2f}".format(pred))
        pos_date = 0
        neg_date = 0
        return float(0.0), pred, pos_date, neg_date

    if date_ == False:
        p_cols = daily_collection.find({"ticker":symbol,"$and":[{"date":{"$lte": tomorrow}}, {"date":{"$gte":yesterday}}]}).sort([("date",DESCENDING)])
    else:
        p_cols = daily_collection.find({"ticker":symbol,"$and":[{"date":{"$lt": tomorrow}}, {"date":{"$gte":yesterday}}]}).sort([("date",DESCENDING)])
    ## sorted from latest to oldest in 3 days range

    try:
        t_cols = p_cols[0] # latest
        t_open = t_cols["open"]
        t_close = t_cols["close"]
        date = t_cols["date"] # readjust datetime
        t_return = (t_close - t_open)/(t_open+t_close)

        y_cols = p_cols[1] # presume that the api will return literally daily
        y_open = y_cols["open"]
        y_close = y_cols["close"]
        yesterday = y_cols["date"] # readjust datetime
        y_return = (y_close - y_open)/(y_open+y_close)

        avg_return  = (t_return + y_return)/2


        ## new datetime adjustment based on alphavantage
        date_date = date.date()
        date_str = date.strftime("%Y-%m-%d 00:00:00")
        date = datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")
        tomorrow = date + timedelta(days=1)
        yesterday = date - timedelta(days=1)

        # tomorrow is in US/Eastern as dt object
        # date is in US/Eastern today as dt object
        # yesterday is in US/Eastern (yesterday) as dt object

        loc_tomorrow = local.localize(tomorrow, is_dst=None)
        loc_date = local.localize(date, is_dst=None)
        loc_yes = local.localize(yesterday, is_dst=None)

        # somehow python magically generate datetime UTC
        utc_tomorrow = loc_tomorrow.astimezone(pytz.utc)
        utc_date = loc_date.astimezone(pytz.utc)
        utc_yes = loc_yes.astimezone(pytz.utc)

        pos_date, neg_date = sentimentAnalysisImpl(symbol, utc_date.date())
        pos_yes, neg_yes = sentimentAnalysisImpl(symbol, utc_yes.date())


        # we thought that aggregate sentiment will be failed somehow
        # because no data with given symbol

        # aggDate = (pos_date-neg_date)/(pos_date+neg_date)
        aggrDate = math.log((1+pos_date)/(1+neg_date))
        # aggYes = (pos_yes-neg_yes)/(pos_yes+neg_yes)

        # avg_aggr = ((pos_date+pos_yes-neg_date-neg_yes)/(pos_date+pos_yes+neg_date+neg_yes))/2#(aggDate+aggYes)/2
        # score = avg_aggr + t_close

        if red.get(symbol+"_regress") == None:
            subprocess.check_call("cd /home/william_hidayat12/event_detection_sentiment_analysis/testing_python; python3 RegressionModelAbstraction.py --symbol="+symbol+" --score="+str(aggrDate), shell=True)
        
        pred = float(red.get(symbol+"_regress"))
        # pred = float(score) 

        price = (float("{0:.2f}".format(pred)) +1)* t_close
        ret = float("{0:.2f}".format(pred))
        pred = price
        
    except Exception:
        # incase of all failures return 0
        pred = float(0.0)
        ret = float(0.0)

        pos_date = 0
        neg_date = 0

    return pred, ret, pos_date, neg_date

# def historicalPriceSentiment(symbol):

#     dateReq = request.args.get("date", None) # manual date parameter

#     date = datetime.now().date()
#     future = False
#     if dateReq == None:
#         forecast_period = 0
#         # assume it is today

#     else:
#         dateReq = datetime.strptime(dateReq, "%Y-%m-%d").date()

#         if dateReq > date:
#             future = True


#     if future: 

#         forecast_period = (dateReq - date).days

#         predictionSentiment = predictSentiment(symbol, forecast_period = forecast_period)

#         predictionSentiment = predictionSentiment["aggregate_sentiment"]

#         price, predictionReturn, pos, neg = newPricePredictionImpl(symbol, score=predictionSentiment)

#         return {"price_prediction": 0, 
#             "price_charts":[], 
#             "return_prediction": predictionReturn, 
#             "aggregate_sentiment": predictionSentiment, 
#             "positives":0, 
#             "negatives":0, 
#             "actual_return":0,
#             "actual_price":0,
#             "sentiment_charts":[]}

#     else:

#         stockCharts = timeSeriesStocksImpl(symbol, dateStr = dateReq.strftime("%Y-%m-%d"), fq="weekly")
#         startDt = dateReq
#         endDt = dateReq + timedelta(days=1)

#         daily_collection = db.stocks_daily
#         cols = daily_collection.find({"ticker":symbol, "$and":[{"date":{"$gte":startDt}},{"date":{"$lt":endDt}}]}).sort([("date", ASCENDING)])
#         cols = cols [0]
#         ret_ = (cols["close"] - cols["open"])/cols["open"]
#         close = cols["close"]

       
#         price, ret, pos, neg = pricePredictionImpl(symbol,date = dateReq, score=None )
#         aggr = math.log((1+pos)/(1+neg))
#         aggr = float("{0:.2f}".format(aggr))
#         return {"price_prediction": price, 
#             "price_charts":stockCharts,
#             "return_prediction": ret_, 
#             "aggregate_sentiment": aggr, 
#             "positives":pos, 
#             "negatives":neg, 
#             "actual_return":ret,
#             "actual_price":close,
#             "sentiment_charts":[]} 

@main.route("/demo")
def demoMode():
    expDate = "2018-03-23"
    dateReq = request.args.get("date", None) # manual date parameter
    if dateReq == None:
        date = "2018-02-07"
    else:
    
        date = datetime.strptime(dateReq, "%Y-%m-%d")
        dateTime = date
        date = date.strftime("%Y-%m-%d")
        expDate = datetime.strptime(expDate, "%Y-%m-%d")
        if dateTime >  expDate:
            return jsonify({"message": "Date should at most 23 March 2018"}), 400
    
    dates, pos_, neg_, charts, close_, ln_, return_ = demo_mode()
    charts_ = []
    for i in range(len(charts)):
        charts_.append({"close":charts[i],"date":dates[i] })
    
    idx = 0
    for i in range(len(dates)):
        if dates[i] == date:
            idx =i 
            break

    if idx <9:
        return jsonify({"message":"Date should at least 7 February 2018"}), 400
    
    pid = idx-1
    res = []
    start = abs(9-pid-1)
    rcoef = [-3.260311e-01, -7.811623e-01, -9.630355e-02, -9.729530e-01, -2.945961e-02 , -1.045966e+00,1.253875e-01, -3.106579e-01, 4.369311e-01]
    scoef = [1.529579e-02, -8.922744e-03, 1.082101e-02 , 1.440055e-02 , 7.721054e-03,  3.044547e-02,-9.880403e-05,6.675528e-03, -2.458155e-02]
    # rcoef = [-0.260381336, -0.777021238, -0.187315437 , -1.187556954, -0.292374531, 1.328191848 , -0.056948199, -0.472076662, 0.415975605 ]
    # scoef = [0.013780231, -0.011023803, 0.006685433, 0.013776678, 0.013440109, 0.035750604, -0.003133597, 0.008783748, -0.024863337]

    lag=0
    latest_closing = 0
    for i in range(pid, start-1, -1):
        if latest_closing == 0:
            latest_closing = close_[i] # closing before the prediction date

        res.append({"lag":lag, "aggr":ln_[i],"return":return_[i], "close":close_[i], "rcoef": rcoef[lag], "scoef": scoef[lag]})
        lag+=1
    latest_price = close_[idx] # closing on the prediction date
    
    next_return = sum([ (i["aggr"]*i["scoef"]+i["return"]*i["rcoef"]) for i in res]) -3.245886e-02 #33: -0.037640278 
    next_close = (next_return +1) * latest_closing

    return jsonify({"charts":charts_, "aggregate_sentiment": ln_[idx], "close":latest_price, "positives":pos_[idx], "negatives":neg_[idx], "prediction_price":next_close, "prediction_return": next_return ,"return":return_[idx]})


@main.route("/shortlist/aggregation/<symbol>")
def shortlistsAggregation(symbol):

    date = datetime.now().date()
    dateStr = date.strftime("%Y-%m-%d")

    """ Present price -- Access Cached """
    timeseries = red.get(symbol)
    timeseries = json.loads(timeseries)
    
    closing = timeseries["close"]
    if "oldest" in timeseries:
        opening = timeseries["oldest"]["open"]
    else:
        opening = timeseries["open"]

    volume = timeseries["volume"]
    timestamp = timeseries["date"]+ " "+ timeseries["minute"] 
    timestamp = datetime.strptime(timestamp,"%Y%m%d %H:%M")

    latest_quote = dict()
    latest_quote["close"] = float(closing)
    latest_quote["open"] = float(opening)
    latest_quote["change"] = (float(closing)- float(opening))/float(opening)*100
    latest_quote["change"] = round(latest_quote["change"],3)
    latest_quote["volume"] = locale.format("%d", float(volume), grouping=True) # make a comma seperated thousands
    """ """

    positives, negatives = sentimentAnalysisImpl(symbol, date)
    sentiments = dict()
    sentiments["positives"] = positives
    sentiments["negatives"] = negatives

    # predicted_price = 100.0
    # pred, ret, pos_date, neg_date

    if red.get("new_pred_"+symbol )==None:
        predicted_price, predicted_return = newPricePredictionImpl(symbol)
        d = dict()
        d["pred_price"] = predicted_price
        d["pred_return"] = predicted_return
        d = json.dumps(d)
        red.set("new_pred_"+symbol, d)
        red.expire("new_pred_"+symbol, 300)

    else:
        d = red.get("new_pred_"+ symbol)
        d= json.loads(d)
        predicted_price = d["pred_price"]
        predicted_return = d["pred_return"]

    if predicted_price == None:
        return jsonify({"price_prediction": float(0.0), "return_prediction": float(0.0), "latest_quote":latest_quote, "sentiment":{"positives":positives, "negatives":negatives}})

    return jsonify({"price_prediction": predicted_price, "return_prediction": predicted_return, "latest_quote":latest_quote, "sentiment":{"positives":positives, "negatives":negatives}})


def newPricePredictionImpl(symbol, date=None):

    # Only works to some of the data

    local = pytz.timezone("US/Eastern")
    date_ = False
    if date == None:
        date = datetime.now().date() # today
    else:
        date_ = True

    date_str = date.strftime("%Y-%m-%d")
    date_str = date_str + " 00:00:00"

    date = datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")
    
    tomorrow  = date + timedelta(days=1) # tomorrow

    daily_collection = db.stocks_daily

    # we need at least 9 days of data


    nineth = date - timedelta(days=13)
    print (nineth)
    p_cols = daily_collection.find({"ticker":symbol,"$and":[{"date":{"$lt": tomorrow}}, {"date":{"$gte":nineth}}]}).sort([("date",DESCENDING)])
    #print (list(p_cols))
    res = []
    lag = 0
    rcoef = [-3.260311e-01, -7.811623e-01, -9.630355e-02, -9.729530e-01, -2.945961e-02 , -1.045966e+00,1.253875e-01, -3.106579e-01, 4.369311e-01]
    scoef = [1.529579e-02, -8.922744e-03, 1.082101e-02 , 1.440055e-02 , 7.721054e-03,  3.044547e-02,-9.880403e-05,6.675528e-03, -2.458155e-02]
    # rcoef = [-0.260381336, -0.777021238, -0.187315437 , -1.187556954, -0.292374531, 1.328191848 , -0.056948199, -0.472076662, 0.415975605 ]
    # scoef = [0.013780231, -0.011023803, 0.006685433, 0.013776678, 0.013440109, 0.035750604, -0.003133597, 0.008783748, -0.024863337]

    ### should be at least 9 observations
    print ("count: ",p_cols.count())
    if p_cols.count() <9:
        return None, None

    latest_closing = 0
    # assume the first is the latest because it's sorted in descending order
    # only take 9 from the latest to the oldest
    for p in range(9):
        i = p_cols[p]
        date = i["date"]
        t_open = i["open"]
        t_close = i["close"]
        if latest_closing ==0:
            latest_closing = t_close
       
        t_return = (t_close - t_open)/(t_open+t_close)
        
        loc_date = local.localize(date, is_dst=None)
        utc_date = loc_date.astimezone(pytz.utc)
        pos_yes, neg_yes = sentimentAnalysisImpl(symbol, utc_date.date())
        aggr = math.log((1+pos_yes)/(1+neg_yes))

        res.append({"lag":lag, "aggr":aggr,"return":t_return, "close":t_close, "rcoef": rcoef[lag], "scoef": scoef[lag]})
        lag+=1
    

    next_return = sum([ (i["aggr"]*i["scoef"]+i["return"]*i["rcoef"]) for i in res]) -3.245886e-02 #33: -0.037640278 
    print ("return ", next_return)
    next_close =  latest_closing + next_return
    return float(next_close), float(next_return)
