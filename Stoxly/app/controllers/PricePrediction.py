import pytz
from datetime import datetime, timedelta
from app.controllers.DashboardController import sentimentAnalysisImpl
from pymongo import MongoClient, DESCENDING
client = MongoClient('10.152.0.3',27017)

db = client.stoxly
collection = db.stocks

def newPricePredictionImpl(symbol, date=None):

    # Only works to some of the data

    local = pytz.timezone("US/Eastern")
    date_ = False
    if date == None:
        date = datetime.now().date() # today
    else:
        date_ = True

    date_str = date.strftime("%Y-%m-%d")
    date_str = date_str + " 00:00:00"

    date = datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")
    
    tomorrow  = date + timedelta(days=1) # tomorrow

    daily_collection = db.stocks_daily

    # we need at least 9 days of data

    nineth = date - timedelta(days=9)

    p_cols = daily_collection.find({"ticker":symbol,"$and":[{"date":{"$lt": tomorrow}}, {"date":{"$gte":nineth}}]}).sort([("date",DESCENDING)])

    res = []
    lag = 0
    rcoef = [-3.260311e-01, -7.811623e-01, -9.630355e-02, -9.729530e-01, -2.945961e-02 , -1.045966e+00,1.253875e-01, -3.106579e-01, 4.369311e-01]
    scoef = [1.529579e-02, -8.922744e-03, 1.082101e-02 , 1.440055e-02 , 7.721054e-03,  3.044547e-02,-9.880403e-05,6.675528e-03, -2.458155e-02]
    ### should be at least 9 observations
    if len(p_cols) <9:
        return 

    latest_closing = 0
    for i in p_cols:

        date = i["date"]
        t_open = i["open"]
        t_close = i["close"]
        if latest_closing ==0:
            latest_closing = t_close
       
        t_return = (t_close - t_open)/(t_open+t_close)
        
        loc_date = local.localize(date, is_dst=None)
        utc_date = loc_date.astimezone(pytz.utc)
        pos_yes, neg_yes = sentimentAnalysisImpl(symbol, utc_date.date())
        aggr = math.log((1+pos_date)/(1+neg_date))

        res.appen({"lag":lag, "aggr":aggr,"return":t_return, "close":t_close, "rcoef": rcoef[lag], "scoef": scoef[lag]})
        lag+=1
    

    next_return = sum([ (i["aggr"]*i["scoef"]+i["return"]*i["rcoef"]) for i in res]) -3.245886e-02 
    next_close = (next_return +1) * latest_closing
    return float(next_close), float(next_return)





    


    