from flask import Blueprint
from flask import render_template, url_for, request, jsonify
from app.models.ShortlistedStocks import ShortlistedStocks
from app.models.Tickers import Tickers
# from app.database import session
from app.database import db
from flask_jwt_extended import (
    jwt_required, get_jwt_identity
)
shortlist = Blueprint('shortlist', __name__)

@shortlist.route("/shortlist")
@jwt_required
def index():
    # s = ShortlistedStocks.query.all()
    
    user_id = get_jwt_identity()
    # print (request.cookies.get("csrf_access_token"))
    token = request.cookies.get("csrf_access_token")
    # session.commit()
    # s = session.query(ShortlistedStocks).filter_by(user_id=user_id)
    s = ShortlistedStocks.query.filter_by(user_id=user_id)
    return render_template('shortlist.html', shortlist = s, token=token)

@shortlist.route("/api/shortlist")
@jwt_required
def shortlist_get_api():
    user_id = get_jwt_identity()
    
    s = ShortlistedStocks.query.filter_by(user_id=user_id)
    symbols = []

    for i in s:
        # print (i.ticker)
        t = Tickers.query.filter_by(symbol = i.ticker)
        if t.count() >0:
            t = t.first()
            symbols.append({"symbol": i.ticker, "company_name": t.title})
        

    return jsonify({"user_id": user_id, "symbols":symbols})
    

@shortlist.route("/shortlist/delete", methods=["POST"])
@jwt_required
def delete():
    user_id = get_jwt_identity()
    
    # user_id = "william.hidayat@gmail.com259ca7d175b5415ebb9a81ec088183f9"

    req = request.get_json(force=True)
    
    if req==None:
        return jsonify({"message":"Invalid request"}), 400    
    
    if "ticker" not in req:
        return jsonify({"message":"Invalid argument"}), 400
    
    ticker = req["ticker"]

    # shortlisted = session.query(ShortlistedStocks).filter_by(user_id=user_id, ticker=ticker)
    shortlisted = ShortlistedStocks.query.filter_by(user_id=user_id, ticker=ticker)

    if shortlisted.count() == 0 :
        return jsonify({"message":"Invalid request"}), 400

    shortlisted = shortlisted.first()
    # session.delete(shortlisted)
    # session.commit()
    db.session.delete(shortlisted)
    db.session.commit()

    return jsonify({"message":"Stock has been removed"}), 201

@shortlist.route("/shortlist", methods=["POST"])
@jwt_required
def add():
    user_id = get_jwt_identity()
    
    req = request.get_json()

    if req==None:
        return jsonify({"message":"Invalid request"}), 400    
    
    if "ticker" not in req:
        return jsonify({"message":"Invalid argument"}), 400
    
    ticker = req["ticker"]

    if "title" not in req:
        title = None # can be null
    else:
        title = req["title"]

    # s = session.query(ShortlistedStocks).filter_by(user_id=user_id, ticker=ticker)
    s = ShortlistedStocks.query.filter_by(user_id=user_id, ticker=ticker)

    if s.count() >0:
        return jsonify({"message":"Stock has been added before"}), 400
    
    stock = ShortlistedStocks(ticker, user_id)
    # session.add(stock)
    # session.commit()

    db.session.add(stock)
    db.session.commit()

    # tickers = session.query(Tickers).filter_by( symbol=ticker)
    tickers = Tickers.query.filter_by(symbol=ticker)
    if tickers.count() == 0:
        ticker = Tickers(symbol=ticker, title=title)
        db.session.add(ticker)
        db.session.commit()

    return jsonify({"message":"Stock added"}), 201
    