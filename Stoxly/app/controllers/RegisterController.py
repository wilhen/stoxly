from flask import Blueprint
from flask import render_template, url_for, request, make_response, redirect, jsonify
from app.models.Users import Users
# from app.database import session
from app.database import db
import hashlib
from uuid import uuid4
from flask_jwt_extended import ( jwt_optional, get_jwt_identity)

register_blueprint = Blueprint('registration', __name__)

@register_blueprint.route("/register")
@jwt_optional
def index():
    if  get_jwt_identity() == None:
        return render_template('registration.html')
    else:
        return make_response(redirect(url_for("main.index")))

@register_blueprint.route("/register", methods=["POST"])
def register():
    
    data = request.form
    first_name = data["first_name"]
    last_name = data["last_name"]
    password =  (hashlib.sha512((data["password"]).encode("utf-8"))).hexdigest() # encrypt password
    email = data["email"]
    # user_id = email+str(uuid4().hex)
    user_id = str(uuid4().hex)
    # uc = session.query(Users).filter_by(email=email).count()
    uc = Users.query.filter_by(email=email).count()

    if uc >0:   
        return jsonify({"message":"Try another email"}) ,400

    try:
        user = Users(user_id=user_id, first_name=first_name, last_name= last_name, password=password, email=email)
        # session.add(user)
        # session.commit()
        db.session.add(user)
        db.session.commit()
        # return jsonify({"message":"Registration completed"}), 201
        return make_response(redirect(url_for("login.index")))
    except Exception:
        return jsonify({"message":"Registration failed"}) ,400

    