from app.commands.Trending import fetch_trending_quotes
from app.commands.Stocks import thread_main
from app.commands.Daily import thread_daily_main
from apscheduler.schedulers.background import BackgroundScheduler
import atexit

def schedule():
    # run command
    print ("running command")
    fetch_trending_quotes()
    
    
def schedule_stocks():
    print ("run stock threads")
    thread_main()

def schedule_stocks_daily():
    print ("run stock threads daily")
    thread_daily_main()

def terminate(scheduler):
    print("kernel process is terminated")
    scheduler.shutdown(wait=False)


def activate_kernel():
    print ("kernel has been activated")

    scheduler = BackgroundScheduler(daemon=True, job_defaults = {'coalesce': False,'max_instances': 10})
    # scheduler.add_job(schedule,"interval",minutes=3)
    scheduler.add_job(schedule_stocks,"interval", minutes=6)
    # scheduler.add_job(schedule_stocks_daily,"cron", hour=17, minute=9, timezone="Australia/Brisbane")
    scheduler.start()

    atexit.register(terminate, scheduler=scheduler)


