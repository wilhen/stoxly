from sqlalchemy import func, Column, String, DateTime
# from app.database import Base
from app.database import db

class Users(db.Model):
    __tablename__ = 'users'
    user_id = db.Column('user_id', db.String(255), primary_key = True)
    email = db.Column('email', db.String(255))
    password = db.Column('password', db.String(255))
    first_name = db.Column('first_name', db.String(255))
    last_name = db.Column('last_name', db.String(255))
    created_at = db.Column("created_at", db.DateTime(), default = func.now())
    updated_at = db.Column("updated_at", db.DateTime(), default = func.now(), onupdate = func.now())

    def __init__(self, user_id, email, password, first_name, last_name):
        self.user_id=user_id
        self.email=email
        self.password=password
        self.first_name = first_name
        self.last_name = last_name
      