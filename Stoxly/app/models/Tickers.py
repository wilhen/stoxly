from sqlalchemy import func, Column, String, DateTime
# from app.database import Base
from app.database import db


class Tickers(db.Model):
    __tablename__ = 'tickers'
    symbol = db.Column('symbol', db.String(10), primary_key = True)
    title = db.Column('title', db.String(255))
    created_at = db.Column("created_at", db.DateTime(), default = func.now())
    updated_at = db.Column("updated_at", db.DateTime(), onupdate = func.now())

    def __init__(self, symbol, title):
        self.symbol = symbol
        self.title = title
      
