# from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func, Column, DateTime, String
from datetime import datetime
# from app.database import Base
from app.database import db
# db = SQLAlchemy()

class ShortlistedStocks(db.Model):
    __tablename__ = 'shortlisted_stocks'
    ticker = db.Column('ticker', db.String(10), primary_key = True)
    user_id = db.Column('user_id', db.String(255), primary_key = True)
    created_at = db.Column("created_at", db.DateTime(), default= func.now())
    updated_at = db.Column("updated_at", db.DateTime(), default = func.now(), onupdate=func.now())

    def __init__(self, ticker, user_id):
        self.ticker = ticker
        self.user_id = user_id
       
