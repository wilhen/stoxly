import redis
from pymongo import MongoClient
import hashlib
from datetime import datetime, timedelta
import urllib3
import json
import threading
# from app.database import session
from app.models.Tickers import Tickers
import time


def fetch_quotes(symbol):
    
    red = redis.StrictRedis(host='localhost', port=6379)

    client = MongoClient('10.152.0.3',27017)

    db = client.stoxly
    collection = db.stocks
    
    API_KEY = "B950NZQDN8X2XDYA"
    url = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol="+symbol+"&interval=5min&outputsize=compact&apikey="+API_KEY
    http = urllib3.PoolManager()
    r = http.request("GET", url)
    
    data = json.loads(r.data)
    timeseries = data["Time Series (5min)"]
    print (symbol+" is fetched")
    ts = timeseries
    datetimes = list(ts.keys())
    # latest_datetime = max(datetimes)
   
    ts = json.dumps(ts)
    
    red.set(symbol, ts)
    red.expire(symbol, 7200) # should be only 4 mins

    for i in datetimes:
        tick = timeseries[i] # i = 2018-04-02 10:15:00
        d = dict()
        d["date"] = datetime.strptime(i, "%Y-%m-%d %H:%M:%S")
        d["ticker"] = symbol
        d["open"] = float(tick["1. open"])
        d["close"] = float(tick["4. close"])
        d["high"] = float(tick["2. high"])
        d["low"] = float(tick["3. low"])
        d["volume"] = tick["5. volume"]

        tid = i+"_"+symbol
        m = hashlib.sha512(tid.encode("utf-8"))
        d["id"] = m.hexdigest()
        if collection.find({"id":d["id"]}).count() ==0:
            collection.insert(d)
            
    client.close()
    # print (data)


def thread_main():
    print ("thread_main is run")
    # tickers  = session.query(Tickers).all()
    tickers  = Tickers.query.all()
    symbols = []
    # symbols = ["AMZN", "F", "TSLA"]

    for s in tickers:
        print (s.symbol)
        symbols.append(s.symbol)

    threads = [threading.Thread(target=fetch_quotes, args=(symbol,)) for symbol in symbols]
    for thread in threads:
        thread.start()
        time.sleep(2)
    for thread in threads:
        thread.join()
