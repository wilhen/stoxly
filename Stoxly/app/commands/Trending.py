import redis
from pymongo import MongoClient
import hashlib
from datetime import datetime, timedelta
import urllib3
import json
from app.models.Tickers import Tickers
# from app.database import session
from app.database import db


red = redis.StrictRedis(host='localhost', port=6379)

def fetch_trending_quotes():

    ACCESS_TOKEN = "97a1992ae3cd49c4fde95a153758153a877efe85" # hide this
    URL_HEADER = "https://api.stocktwits.com/api/2/trending/symbols.json"
    url = URL_HEADER+"?access_token="+ACCESS_TOKEN
    http = urllib3.PoolManager()
    print ("fetch_trending_quotes is run")
    r = http.request("GET", url)
    data = json.loads(r.data)
    symbols = data["symbols"]

    markets = ["NYSE","DJIA", "SPY","SPX","IXIC" ,"IWM", "QQQ", "LABU", "TQQQ", "DIA", "VIX", "TVIX" ,"TLT","UVXY", "IBB", "EEM"]
    trendings = []
   
    for i in range(8):
        d = dict()
        if ("." in symbols[i]["symbol"]) or (symbols[i]["symbol"] in markets):
            continue # such as BTC.X
        d["symbol"]= symbols[i]["symbol"]
        d["title"] = symbols[i]["title"]
        d["id"] = symbols[i]["id"]
        trendings.append(d)

        # brute store long time in database
        # session.commit()
        # s = session.query(Tickers).filter_by(symbol=d["symbol"]).count()
        s = Tickers.query.filter_by(symbol=d["symbol"]).count()

        if s == 0:
            tickers = Tickers(symbol=d["symbol"], title=d["title"])
            # session.add(tickers)
            # session.commit()
            db.session.add(tickers)
            db.session.commit()

    trendings = json.dumps(trendings)
    red.set("trending", trendings) # make fast interface
    red.expire('trending', 240) # 4 minutes
