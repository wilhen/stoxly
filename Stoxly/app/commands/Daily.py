import redis
from pymongo import MongoClient
import hashlib
from datetime import datetime, timedelta
import urllib3
import json
import threading
# from app.database import session
from app.models.Tickers import Tickers
import time

def fetch_daily_quotes(symbol):
    
    client = MongoClient('10.152.0.3',27017)

    db = client.stoxly
    collection = db.stocks_daily
    
    API_KEY = "B950NZQDN8X2XDYA"
    url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol="+symbol+"&outputsize=compact&apikey="+API_KEY
    http = urllib3.PoolManager()
    r = http.request("GET", url)
    
    data = json.loads(r.data)
    ts = data["Time Series (Daily)"]
    print (symbol+" is fetched (daily)")

    keys = list(ts.keys())
   

    for i in keys:
        tick = ts[i] # i = 2018-04-02 10:15:00
        d = dict()
        d["date"] = datetime.strptime(i, "%Y-%m-%d")
        d["ticker"] = symbol
        d["open"] = float(tick["1. open"])
        d["close"] = float(tick["4. close"])
        d["high"] = float(tick["2. high"])
        d["low"] = float(tick["3. low"])
        d["volume"] = tick["5. volume"]

        tid = i+"_"+symbol
        m = hashlib.sha512(tid.encode("utf-8"))
        d["id"] = m.hexdigest()
        if collection.find({"id":d["id"]}).count() ==0:
            collection.insert(d)

    # print (data)


def thread_daily_main():
    print ("thread_daily_main is run")
    # tickers  = session.query(Tickers).all()
    tickers  = Tickers.query.all()
    symbols = []
    # symbols = ["WMT", "AXP","MCD","CVX","CAT","NKE","F","XOM"]

    for s in tickers:
        print (s.symbol)
        symbols.append(s.symbol)
    
    for symbol in symbols:
        fetch_daily_quotes(symbol)

    # threads = [threading.Thread(target=fetch_daily_quotes, args=(symbol,)) for symbol in symbols]
    # for thread in threads:
    #     thread.start()
        
    # for thread in threads:
    #     thread.join()
