$(document).ready(function(){
    
    $(".shortlisted-stock-chart").each(function(index){
        var tickers = ["SNAP","TSLA"];
        var id = this.id;
        generateChart(id, id)
    });

    $(".frequency").click(function(){
        console.log(this.id);
        var idSplit = this.id.split("-")
        var selectedTicker = idSplit[0];
        var fq = idSplit[2];
        // clean up the svg
        $("#"+selectedTicker).empty();
       
        
        console.log(selectedTicker)
        writeChart(selectedTicker, selectedTicker, fq)
        // generateD3(selectedTicker, "SNAP", fqText)
    });

    $("#add-stock-box").on("input", function(){
        console.log($(this).val());
        $(".add-stock").addClass("disabled")
    });

    $(".add-stock").click(function(){
        var value = $("#add-stock-box").val();
        var stock_data = window._stock;
        // console.log(value)
        $.ajax({
            url: '/shortlist',
            type: 'POST',
            data:JSON.stringify({ticker:value, title: stock_data.Text}),
            headers:{
                "X-CSRF-TOKEN":window._token
            },
            dataType:"json",
            contentType:"application/json; charset=utf-8",
            success: function(result) {
                // Do something with the result
                console.log(result);
                // $("#"+ticker+"-row").remove();
                $("#shortlisted-stocks").append("<tr id=\""+value+"-row\">"
                                                +"<td><a href=\"#\">"+value+"</a></td>"
                                                +"<td class=\"font-grey\">yesterday</td>"
                                                +"<td>positive</td>"
                                                +"<td class=\"font-grey\">10/9/2018</td>"
                                                +"<td class=\"font-grey\"><button class=\"ui red button delete-stock\" id=\""+value+"-delete\">Delete</button></td>"
                                                +"</tr>");     
            },
            error:function(jqXHR, textStatus, errorThrown){
                console.error(jqXHR.responseJSON);
            }
        });
    });
    
    $(document).on("click",".delete-stock",function(){
      
        var ticker =this.id.split("-")[0];
       
        $.ajax({
            url: '/shortlist/delete',
            type: 'POST',
            data:JSON.stringify({ticker:ticker}),
            headers:{
                "X-CSRF-TOKEN":window._token
            },
            dataType:"json",
            contentType:"application/json; charset=utf-8",
            success: function(result) {
                // Do something with the result
                console.log(result);
                $("#"+ticker+"-row").remove();
            },
            error:function(jqXHR, textStatus, errorThrown){
                console.error(jqXHR.responseJSON);
            }
        });
    })

    $(".sentiment").click(function(){
      
        var id =this.id.split("-")[0];
        var sentiment = this.id.split("-")[1];
        $.ajax({
            url: '/sentiment',
            type: 'PUT',
            data:JSON.stringify({id:id, sentiment:sentiment}),
            headers:{
                "X-CSRF-TOKEN":window._token
            },
            dataType:"json",
            contentType:"application/json; charset=utf-8",
            success: function(result) {
                // Do something with the result
                console.log(result);
                $("#sentiment-text-"+id).text(sentiment);
                $("#status-"+id).text("True");
            },
            error:function(jqXHR, textStatus, errorThrown){
                console.error(jqXHR.responseJSON);
            }
        });
    });

    $(".spam").click(function(){
      
        var id =this.id.split("-")[0];
       
        $.ajax({
            url: '/spam',
            type: 'PUT',
            data:JSON.stringify({id:id, spam:true}),
            headers:{
                "X-CSRF-TOKEN":window._token
            },
            dataType:"json",
            contentType:"application/json; charset=utf-8",
            success: function(result) {
                // Do something with the result
                console.log(result);
                $("#sentiment-text-"+id).text("spam");
                $("#status-"+id).text("True");
            },
            error:function(jqXHR, textStatus, errorThrown){
                console.error(jqXHR.responseJSON);
            }
        });
    });

    $(document).on("click","#query-tweets",function(){
        var date = $("#date").val();

        if (date){
            var isActive = $('#active:checkbox:checked').length > 0;
            if (isActive){
                isActive = 1
            } else{
                isActive = 0
            }
            window.location.href = "/sentiment/dashboard?date="+date+"&active="+isActive;
        }else{
            window.alert("Please select a date");
        }
       
  
    });
  

});