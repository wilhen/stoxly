$(document).ready(function(){
    $.get("/trending", function(res){

        console.log(res);  
        var guard = 6;                  
        res.forEach(element => {

            if (guard<=0){
                return;
            }
            
            var r = element
            getStockDetail(r.symbol, function(stockDetail){
                var change = (parseFloat(stockDetail[2])-parseFloat(stockDetail[1])).toFixed(3);
                var color =  (change<0 ) ? "red" : "green";

                $("#trending-body").append("<tr><td><h4><a href=\"#\">"+stockDetail[0]+"</a></h4></td>"+
                    "<td class=\"font-grey\">"+stockDetail[2]+"</td><td><span style=\"color:"+color+";\">"+
                    change+"</span></td><td><span style=\"color:"+color+";\">"+
                    stockDetail[3]+" %</span></td>"
                    // +"<td class=\"font-grey\">"+stockDetail[3]+"</td></tr>"
                );
            });
            guard-=1;
            
        });
        
    });

    function getStockDetail(sym, handleData){
        console.log(sym)
        
        $.get("/quote/"+sym, function(res){
            // res = JSON.parse(res);
            // var low = res["3. low"];
            // var high = res["2. high"];
            var open =  res["1. open"];
            var close = res["4. close"];
            // var volume = res ["5. volume"];
            var change = res ["6. change"]
            var data = [sym, open, close, change]
            handleData(data)
        });

    }
});
