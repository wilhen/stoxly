function generateChart(ticker, id){
    AmCharts.ready(function () {
        
        // writeChart(ticker,id, null)
    });
}

function writeChart(ticker,id, fq){
    generateChartData(ticker, fq,function(data){
        createStockChart(data, id);
    });
}

function generateChartData(ticker,fq, handler) {

    var chartData = [];
    // var frequency = null;
    var link = "/prices/"+ticker;
    if (fq == "1m"){
        // frequency = "monthly";
        link = link+"?frequency=monthly"
    }else if (fq=="5d"){
        
        link = link+"?frequency=weekly"
    }
    // else{
    //     link = link+"?date=2018-04-06"
    // }
    
    $.getJSON(link, function(data){
        var parseTime = d3.timeParse("%Y-%m-%d %H:%M:%S");
        data.forEach(element => {
            element.date = parseTime(element.date);
            element.close = parseFloat(element.close);
            if (fq=="5d"){
                console.log(element)    
            }
           
            chartData.push({
                date: element.date,
                value: element.close
            })
        });
        handler(chartData)
    })
}


var chart;

function createStockChart(chartData, elemID) {
    // console.log(chartData)
    chart = new AmCharts.AmStockChart();

    // As we have minutely data, we should set minPeriod to "mm"
    var categoryAxesSettings = new AmCharts.CategoryAxesSettings();
    categoryAxesSettings.minPeriod = "mm";
    chart.categoryAxesSettings = categoryAxesSettings;

    // DATASETS //////////////////////////////////////////
    var dataSet = new AmCharts.DataSet();
    dataSet.color = "#b0de09";
    dataSet.fieldMappings = [{
        fromField: "value",
        toField: "value"
    }, 
    
    ];
    dataSet.dataProvider = chartData;
    dataSet.categoryField = "date";

    // set data sets to the chart
    chart.dataSets = [dataSet];

    // PANELS ///////////////////////////////////////////
    // first stock panel
    var stockPanel1 = new AmCharts.StockPanel();
    stockPanel1.showCategoryAxis = false;
    stockPanel1.title = "Value";
    stockPanel1.percentHeight = 70;

    // graph of first stock panel
    var graph1 = new AmCharts.StockGraph();
    graph1.valueField = "value";
    graph1.type = "smoothedLine";
    graph1.lineThickness = 2;
    graph1.bullet = "round";
    graph1.bulletBorderColor = "#FFFFFF";
    graph1.bulletBorderAlpha = 1;
    graph1.bulletBorderThickness = 3;
    stockPanel1.addStockGraph(graph1);

    // create stock legend
    var stockLegend1 = new AmCharts.StockLegend();
    stockLegend1.valueTextRegular = " ";
    stockLegend1.markerType = "none";
    stockPanel1.stockLegend = stockLegend1;

    chart.panels = [stockPanel1];


    // OTHER SETTINGS ////////////////////////////////////
    var scrollbarSettings = new AmCharts.ChartScrollbarSettings();
    scrollbarSettings.graph = graph1;
    scrollbarSettings.usePeriod = "10mm"; // this will improve performance
    scrollbarSettings.updateOnReleaseOnly = false;
    scrollbarSettings.position = "top";
    chart.chartScrollbarSettings = scrollbarSettings;

    var cursorSettings = new AmCharts.ChartCursorSettings();
    cursorSettings.valueBalloonsEnabled = true;
    chart.chartCursorSettings = cursorSettings;


    // PERIOD SELECTOR ///////////////////////////////////
    var periodSelector = new AmCharts.PeriodSelector();
    periodSelector.position = "top";
    periodSelector.dateFormat = "YYYY-MM-DD JJ:NN";
    periodSelector.inputFieldWidth = 150;
    periodSelector.periods = [{
        period: "hh",
        count: 1,
        label: "1 hour"
    }, {
        period: "hh",
        count: 2,
        label: "2 hours"
    }, {
        period: "hh",
        count: 5,
        label: "5 hour"
    }, {
        period: "hh",
        count: 12,
        label: "12 hours"
    }, {
        period: "MAX",
        label: "MAX"
    }];
    chart.periodSelector = periodSelector;

    var panelsSettings = new AmCharts.PanelsSettings();
    panelsSettings.mouseWheelZoomEnabled = true;
    panelsSettings.usePrefixes = true;
    chart.panelsSettings = panelsSettings;

    // console.log(elemID)
    chart.write(elemID);
}

