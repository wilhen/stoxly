from flask import Flask
from app.controllers.DashboardController import main
from app.controllers.LoginController import login_blueprint, jwt, error_utils
from app.controllers.RegisterController import register_blueprint
from app.controllers.ShortlistController import shortlist
from app.controllers.ToolsController import tools
from app.kernel import activate_kernel
from app.database import db
# from app.models.ShortlistedStocks import db
# from app.models.Tickers import db

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:Notesadmin#@localhost:3306/stoxly'
# Setup the Flask-JWT-Extended extension
app.config['JWT_SECRET_KEY'] = 'nEce&Pumu7rE'  
app.config['JWT_TOKEN_LOCATION'] = ['cookies', "headers"]
app.config['JWT_COOKIE_CSRF_PROTECT'] = True # to avoid CSRF attack

app.register_blueprint(main)
app.register_blueprint(login_blueprint)
app.register_blueprint(register_blueprint)
app.register_blueprint(shortlist)
app.register_blueprint(tools)

jwt.init_app(app)
jwt.invalid_token_loader(error_utils)
jwt.expired_token_loader(error_utils)
jwt.unauthorized_loader(error_utils)
jwt.revoked_token_loader(error_utils)

db.init_app(app)

@app.before_first_request
def activate():
    activate_kernel()

