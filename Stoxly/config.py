DEBUG = True # Turns on debugging features in Flask
BCRYPT_LOG_ROUNDS = 12 # Configuration for the Flask-Bcrypt extension
MAIL_FROM_EMAIL = "robert@example.com" # For use in application emails

# Define the application directory
import os
BASE_DIR = os.path.abspath(os.path.dirname(__file__))  

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED  = True

# Use a secure, unique and absolutely secret key for
# signing the data. 
CSRF_SESSION_KEY = "secret"

# Secret key for signing cookies
SECRET_KEY = "secret"