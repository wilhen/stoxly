from pymongo import MongoClient
import json
import hashlib
from datetime import datetime
with open("/Users/williamhenry/Documents/web_python/Stoxly/app/static/js/TSLA_temp.json") as f:
    quotes = json.load(f)

print (quotes)
client = MongoClient('localhost',27017)

db = client.stoxly
collection = db.stocks

keys = list(quotes.keys())
symbol = "TSLA"
for k in keys:
    tick = quotes[k]
    d = dict()
    d["date"] = datetime.strptime(k, "%Y-%m-%d %H:%M:%S")
    d["ticker"] = symbol
    d["open"] = float(tick["1. open"])
    d["close"] = float(tick["4. close"])
    d["high"] = float(tick["2. high"])
    d["low"] = float(tick["3. low"])
    d["volume"] = tick["5. volume"]

    tid = k+"_"+symbol
    m = hashlib.sha512(tid.encode("utf-8"))
    d["id"] = m.hexdigest()
    if collection.find({"id":d["id"]}).count() ==0:
        collection.insert(d)
